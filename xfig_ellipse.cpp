// -*-c++-*-

// fixg2sxd - a utility to convert fig to sxd format

// Copyright (C) 2003-2022 Alexander Bürger, acfb@users.sourceforge.net

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation; either version 2 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

#include "xfigobjects.h"

#include "keep_range.h"
#include "misc.h"
#include "xmlwrite.h"

istream& Ellipse::read( istream& figfile )
{
    LineFillStyle lfstmp;
    lfstmp.read( figfile, sub_type, depth );
    int direction;
    figfile >> direction >> angle >> center_x >> center_y >> radius_x
            >> radius_y >> start_x >> start_y >> end_x >> end_y;

    keep_range( sub_type,  "ellipse sub_type",  1, 4 );
    // direction should be always 1, but apparently it is sometimes 0;
    // don't complain as it is not used anyhow
    //keep_range( direction, "ellipse direction", 1, 1 );
    keep_range( radius_x,  "ellipse radius_x",  0.0f );
    keep_range( radius_y,  "ellipse radius_y",  0.0f );

    lfs = linefillstyles.insert( lfstmp ).first;
    lfs->stylename();

    return figfile;
}

ostream& Ellipse::write( ostream& out )
{
    // skip invisible object
    if( !( lfs->hasLine() || lfs->hasFill() ) )
        return out;

    Node ellipse("draw:ellipse");
    ellipse["draw:style-name"] << lfs->stylename();
    ellipse["draw:z-index"] << depth2z(depth);
    if(angle==0) {
        ellipse["svg:x"] << tr(center_x-radius_x) << "cm";
        ellipse["svg:y"] << tr(center_y-radius_y) << "cm";
    } else {
        ellipse["draw:transform"]
            << "rotate(" << angle << ") "
            << "translate(" << tr(center_x-cos(angle)*radius_x-sin(angle)*radius_y) << "cm "
            <<                 tr(center_y+sin(angle)*radius_x-cos(angle)*radius_y) << "cm)";
    }
    ellipse["svg:width"]   << 2*tr(radius_x) << "cm";
    ellipse["svg:height"]  << 2*tr(radius_y) << "cm";
    return out << ellipse;
}
