// -*-c++-*-

// fixg2sxd - a utility to convert fig to sxd format

// Copyright (C) 2003-2022 Alexander Bürger, acfb@users.sourceforge.net

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation; either version 2 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

#ifndef MISC_H
#define MISC_H

#include "vector2.h"
#include <istream>
#include <ostream>

extern int resolution;

/** Convert from xfig units (points per inch) to cm. */
float tr(float xfigunits);
struct ostream_tr {
    Vector2f v;
    ostream_tr( Vector2f const& vv ) : v( vv ) { }
};

std::ostream& operator<<(std::ostream& out, ostream_tr const& o);

inline ostream_tr tr(Vector2f const& xfigunits)
{ return ostream_tr(xfigunits); }

float tr80(float xfigunits);

int tr_p(float xfigunits);
struct ostream_tr_p {
    Vector2f v;
    ostream_tr_p( Vector2f const& vv ) : v( vv ) { }
};

std::ostream& operator<<(std::ostream& out, ostream_tr_p const& o);

inline ostream_tr_p tr_p(Vector2f const& xfigunits)
{ return ostream_tr_p(xfigunits);}

void fail( const char* msg );

void skip_comment( std::istream& figfile );

extern int depth_max;

int depth2z(int depth);

#endif // MISC_H
