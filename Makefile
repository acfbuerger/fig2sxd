
# fixg2sxd - a utility to convert fig to sxd format

# Copyright (C) 2003-2022 Alexander Bürger, acfb@users.sourceforge.net

# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation; either version 2 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

# to cross-compile for win32, try: make CXX=i586-mingw32msvc-g++
# INCLUDES=-I/tmp/zlib-win32/include LIBS=/tmp/zlib-win32/lib/libz.a

# to put binaries into separate directory (good for cross-compile), go
# there and try: make -f ..srcdir../Makefile where ..srcdir.. is the
# path to the source directory

DESTDIR =
PREFIX = /usr
SRCDIR := $(dir $(lastword $(MAKEFILE_LIST)))

TARGET1 = fig2sxd$(EXE)
HEADERS1 = colors.h misc.h styles.h xfigobjects.h xmlwrite.h vector2.h \
	zipwrite.h
SOURCES1 = fig2sxd.cpp xfig_arc.cpp xfig_compound.cpp xfig_ellipse.cpp \
	xfig_poly.cpp xfig_spline.cpp xfig_text.cpp \
	style_arrow.cpp style_line.cpp style_text.cpp colors.cpp misc.cpp \
	xmlwrite.cpp zipwrite.cpp keep_range.cpp
OBJECTS1 = $(SOURCES1:%.cpp=%.o)

TARGETS = $(TARGET1)

DEPEND = .depend-
DEPFILES = $(SOURCES1:%.cpp=$(DEPEND)%.dep)


CXX = g++
CXXFLAGS = -Wall -W -O2 -g
CXXFLAGS += $(INCLUDES)
LIBS = -lz

$(TARGET1): $(OBJECTS1)
	$(CXX) $(LDFLAGS) -o $@ $^ $(LIBS)

$(OBJECTS1): %.o: $(SRCDIR)%.cpp $(DEPEND)%.dep
	$(CXX) $(CXXFLAGS) -o $@ -c $<

$(DEPFILES): $(DEPEND)%.dep: $(SRCDIR)%.cpp
	$(CXX) $(INCLUDES) -M $< > $@

-include $(DEPFILES)

clean:
	rm -f $(TARGETS) $(OBJECTS1)

veryclean: clean
	rm -f $(DEPFILES)

all: $(TARGET1)

install: $(TARGET1) $(SRCDIR)/fig2sxd.1
	install -d $(DESTDIR)$(PREFIX)/bin $(DESTDIR)$(PREFIX)/share/man/man1
	install -m755 $(TARGET1) $(DESTDIR)$(PREFIX)/bin
	install -m644 $(SRCDIR)/fig2sxd.1 $(DESTDIR)$(PREFIX)/share/man/man1

.PHONY: all clean
