// -*-c++-*-

// fixg2sxd - a utility to convert fig to sxd format

// Copyright (C) 2003-2022 Alexander Bürger, acfb@users.sourceforge.net

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation; either version 2 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

#include "keep_range.h"
#include "xfigobjects.h"
#include "misc.h"
#include "xmlwrite.h"

#include <cmath>

istream& Arc::read( istream& figfile )
{
    LineFillStyle lfstmp;
    lfstmp.read( figfile, sub_type, depth );
    figfile >> cap_style >> direction >> forward_arrow >> backward_arrow
            >> center_x >> center_y >> x1 >> y1 >> x2 >> y2 >> x3 >> y3;

    keep_range( sub_type,  "arc sub_type",  1, 2 );
    keep_range( cap_style, "arc cap_style", 0, 2 );
    keep_range( direction, "arc direction", 0, 1 );
    keep_range( forward_arrow,  "arc forward_arrow",  0, 1 );
    keep_range( backward_arrow, "arc backward_arrow", 0, 1 );

    if( forward_arrow != 0 )
        lfstmp.read_arrow( figfile, direction == 0 );
    if( backward_arrow != 0 )
        lfstmp.read_arrow( figfile, direction != 0 );
    lfs = linefillstyles.insert( lfstmp ).first;
    lfs->stylename();
    lfs->stylename_line();
    lfs->stylename_fill();

    return figfile;
}

ostream& Arc::write( ostream& out )
{
    float radius = sqrt( (center_x - x1)*(center_x - x1)
                         + (center_y - y1)*(center_y - y1) );
    // y increases to bottom => minus sign
    float angle_start = -atan2( y1-center_y, x1-center_x ) * 180/M_PI;
    float angle_end   = -atan2( y3-center_y, x3-center_x ) * 180/M_PI;
    if( direction == 0 )
        swap( angle_end, angle_start );
    Node circle("draw:circle");
    circle["draw:z-index"] << depth2z(depth);
    circle["svg:x"] << tr(center_x-radius) << "cm";
    circle["svg:y"] << tr(center_y-radius) << "cm";
    //circle["svg:r"]  << tr(radius) << "cm";
    circle["svg:width"]  << 2*tr(radius) << "cm";
    circle["svg:height"]  << 2*tr(radius) << "cm";
    circle["draw:start-angle"] << angle_start;
    circle["draw:end-angle"]   << angle_end;
    if( sub_type == 2 ) {
        if( lfs->hasLine() || lfs->hasFill() ) {
            circle["draw:style-name"] << lfs->stylename();
            circle["draw:kind"] << "section";
            out << circle;
        }
    } else {
        if( lfs->hasFill() ) {
            circle["draw:style-name"] << lfs->stylename_fill();
            circle["draw:kind"] << "cut";
            out << circle;
        }
        if( lfs->hasLine() ) {
            circle["draw:style-name"] << lfs->stylename_line();
            circle["draw:kind"] << "arc";
            out << circle;
        }
    }
    return out;
}
