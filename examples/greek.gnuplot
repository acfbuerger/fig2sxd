set term epslatex
set size 0.5,0.5
set out "greek_gnuplot.eps"
set ylabel "$\\int_{-\\infty}^a$"
set xlabel "a"
plot [-1:-0.1] -x**-2
set term dumb
