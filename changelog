2015-04-04 Alexander Bürger <acfb@users.sourceforge.net>

	* version 0.22.1

	* index-out-of-range problems had been pointed out by
	  Jodie Cunningham <jodie.cunningham@gmail.com> -- thank you!


2015-02-28 Alexander Bürger <acfb@users.sourceforge.net>

	* version 0.22

	* check for out-of-range text justification and colors


2010-05-19 Alexander Bürger <acfb@users.sourceforge.net>

	* version 0.21

	* add -stylebase option to allow including several figures
	in one document without messing up the styles (nb: OLE!)

	* remove end of line after mimetype; this made OOo 3.2 claim
	that files are corrupted and unrepairable; closes sf.net #3004139


2008-10-25 Alexander Bürger <acfb@users.sourceforge.net>

	* version 0.20

	* print warnings for long polygons, polylines and splines


2008-10-17 Alexander Bürger <acfb@users.sourceforge.net>

	* version 0.19

	* Split long unfilled polylines to avoid problems with
	OpenOffice.org being apparently unable to read xml attribute
	values longer than 64kB.

	* Accept float coordinates (although xfig specifies only
	integers).

2008-05-22 Alexander Bürger <acfb@users.sourceforge.net>

	* version 0.18

	* Set numeric locale to "C" at startup.

	* Fix text placement problems by explicitly setting padding to
	0cm.

2007-05-16 Alexander Bürger <acfb@users.sourceforge.net>

	* version 0.17

	* Add includes for gcc 4.3 and other compilers. (closes: debian
	#417183)

	* Support writing to stdout. Thanks for suggesting to Duncan
	Simpson <duncan@oxbridgeapps.com>.

2006-11-22 Alexander Bürger <acfb@users.sourceforge.net>

	* version 0.16

	* Fixed problem described in sf.net bug report #1563135 by
	skipping more comments. Comments between lines of one object still
	cause problems.

2006-11-21 Alexander Bürger <acfb@users.sourceforge.net>

	* version 0.15

	* Improved compatibility with OOo 2.

	* Fixed wrong font name for Symbol font.

2006-02-08 Alexander Bürger <acfb@users.sourceforge.net>

	* version 0.14

	* Added range-checks for most/all? values in the input as
	specified in xfig's FORMAT3.2. Now it does not crash any longer
	for the schemat.fig example of jfig. Thanks to Patrice
	Moreaux (LISTIC, Universite de Savoie, France) for pointing me at
	this problem.

2004-07-30 Alexander Bürger <acfb@users.sourceforge.net>

	* version 0.13

	* Added some #include <cmath> to get sqrt with g++-3.4. (closes:
	debian bug #262380)

2004-04-29 Alexander Bürger <acfb@users.sourceforge.net>

	* version 0.12

	* Fixed infinite loop when reading files with multi-line comments.

2004-04-20 Alexander Bürger <acfb@users.sourceforge.net>

	* version 0.11

	* Fixed bug in placement of rotated ellipses.

	* Very basic support for LaTeX fonts.

	* Tried to improve text placement for very small font sizes.

2004-01-21 Alexander Bürger <acfb@users.sourceforge.net>

	* version 0.9

	* Tried to make zipwrite independent of endianness.

2004-01-16 Alexander Bürger <acfb@users.sourceforge.net>

	* version 0.8

	* Write meta.xml, mimetype and manifest.xml; this makes including
	the output in other OOo documents much easier.

2004-01-15 Alexander Bürger <acfb@users.sourceforge.net>

	* version 0.7

	* Add short option for linewidth.

	* Position splines much better. Rest of malpositioning comes from
	different shape of the splines.

2004-01-14 Alexander Bürger <acfb@users.sourceforge.net>

	* version 0.6

	* Fix bug in x-splines placing them completely wrong.

	* Do not write control characters (below ' ').

	* Add option to set line thickness for thickness 1.

2004-01-14 Alexander Bürger <acfb@users.sourceforge.net>

	* version 0.5

	* Skip invisible lines/fills.

2004-01-11 Alexander Bürger <acfb@users.sourceforge.net>

	* version 0.4

	* Better placement for font sizes < 7.

	* Support for "Helvetica-Narrow".

	* Stop using different layers for line and fill in filled, open
	objects.

	* Support for more than 1000 layers.

2004-01-07 Alexander Bürger <acfb@users.sourceforge.net>

	* version 0.3

	* Guess output filename when just input file is given.

2004-01-03 Alexander Bürger <acfb@users.sourceforge.net>

	* version 0.2

	* Write sxd file directly.

2003-12-28 Alexander Bürger <acfb@users.sourceforge.net>

	* version 0.1

	* Initial Release.
