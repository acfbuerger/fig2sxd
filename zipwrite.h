// -*-c++-*-

// fixg2sxd - a utility to convert fig to sxd format

// Copyright (C) 2003-2022 Alexander Bürger, acfb@users.sourceforge.net

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation; either version 2 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

#ifndef ZIPWRITE_H
#define ZIPWRITE_H

#include <ios>
#include <fstream>
#include <ostream>
#include <string>
#include <vector>
#include <zlib.h>

class ZipWriter {
    struct fileinfo {
        unsigned short last_mod_file_time;
        unsigned short last_mod_file_date;
        unsigned int crc32;
        unsigned int compressed_size;
        unsigned int uncompressed_size;
        unsigned short file_name_length;
        unsigned int rel_offset_local_header;

        std::string file_name;
    };

    typedef std::vector<fileinfo> fileinfos_t;
    fileinfos_t fileinfos;
    std::ostream& zip;
    std::ostream* out;
    z_stream zs;
    char cbuf[32768];
public:
    ZipWriter( std::ostream& z )
        : zip( z ),
          out(0)
        { }

    std::ostream& GetStream( const char* filename );
    ~ZipWriter();
private:
    void Close();
    int Write( const char* buf, unsigned length );
    static int writefunc( void* user, const char* buf, unsigned length );
};

#endif // ZIPWRITE_H
